import QtQuick 2.4
import Ubuntu.Components 1.3

Item {
    id: empty
    anchors.fill: parent

    visible: fixtureModel.count == 0
    z: 1

    Label {
        id: title
        // TRANSLATORS: Original German quote - "Ein Tag ohne Fußball ist ein verlorener Tag."
        text: i18n.tr("»A day without football is a day lost.«")
        wrapMode: Text.WordWrap
        horizontalAlignment: Text.AlignHCenter
        fontSize: "large"
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
            margins: units.gu(4)
        }
    }

    Label {
        id: subtitle
        text: "Ernst Happel"
        wrapMode: Text.WordWrap
        horizontalAlignment: Text.AlignHCenter
        fontSize: "medium"
        anchors {
            top: title.bottom
            left: parent.left
            right: parent.right
            margins: units.gu(1)
        }
    }

    Item {
        anchors {
            top: subtitle.bottom
            bottom: parent.bottom
            left: parent.left
            right: parent.right
            margins: units.gu(2)
        }

        Image {
            id: image
            source: "image://thumbnailer/" + Qt.resolvedUrl("../graphics/empty_goal.svg")
            anchors.fill: parent
            sourceSize: Qt.size(1024, 1024)
            fillMode: Image.PreserveAspectFit
            asynchronous: true
            antialiasing: true
        }
    }
}

